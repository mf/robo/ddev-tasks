<?php

namespace MFrieling\Robo\Task\DDev;

use Robo\Result;

/**
 * Trait loadTasks
 *
 * @package MFrieling\Robo\Task\DDev
 */
trait loadTasks {

  /**
   * Check if DDEV is installed on the current system.
   *
   * @return \MFrieling\Robo\Task\DDev\DDevExists
   *    DDEV Exists task
   */
  protected function taskDDevExists() {
    return $this->task(DDevExists::class);
  }
}
