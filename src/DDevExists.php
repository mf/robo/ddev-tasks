<?php
/**
 * Created by PhpStorm.
 * User: mfrieling
 * Date: 08.01.2019
 * Time: 19:05
 */

namespace MFrieling\Robo\Task\DDev;

use Robo\Result;

class DDevExists implements \Robo\Contract\TaskInterface {

  /**
   * @return Result
   */
  public function run() {
    try {
      $output = NULL;
      $return_var = NULL;
      $result = exec('which ddev', $output, $return_var);
      return Result::success($this);
    } catch (\Exception $ex) {
      return Result::error($this, 'Could not check if DDEV exists due to an unrecoverable error: ' . $ex->getMessage());
    }
  }
}
