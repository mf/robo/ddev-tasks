<?php

use League\Container\ContainerAwareInterface;
use League\Container\ContainerAwareTrait;
use MFrieling\Robo\Task\DDev\loadTasks;
use PHPUnit\Framework\TestCase;
use Robo\Collection\CollectionBuilder;
use Robo\Robo;
use Robo\TaskAccessor;
use Robo\Tasks;
use Symfony\Component\Console\Output\NullOutput;

class DDevExistsTaskTest extends TestCase implements ContainerAwareInterface {

  use loadTasks;
  use TaskAccessor;
  use ContainerAwareTrait;

  /**
   * @inheritdoc
   */
  function setUp() {
    parent::setUp();
    $container = Robo::createDefaultContainer(NULL, new NullOutput());
    $this->setContainer($container);
  }

  /**
   * Scaffold collection builder.
   *
   * @return \Robo\Collection\CollectionBuilder
   *    Collection builder.
   */
  public function collectionBuilder()
  {
    $emptyRobofile = new Tasks;
    return CollectionBuilder::create($this->getContainer(), $emptyRobofile);
  }

  public function testDDevExists() {
    $result = $this->taskDDevExists()
      ->run();
  }
}
